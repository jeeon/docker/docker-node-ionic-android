#
# Use Jeeon's node-ionic image
#

FROM jeeon/node-ionic:10-alpine

include(common/maintainer.dockerfile)

include(common/install-openjdk-alpine.dockerfile)

include(common/install-gradle.dockerfile)

include(common/install-android.dockerfile)
