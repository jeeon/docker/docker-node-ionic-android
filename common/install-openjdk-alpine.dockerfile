#
# Install OpenJDK and OpenSSL
#

ENV EDGE_MAIN_REPOSITORY=http://dl-cdn.alpinelinux.org/alpine/edge/main \
    EDGE_COMMUNITY_REPOSITORY=http://dl-cdn.alpinelinux.org/alpine/edge/community \
    \
    OPENJDK_VERSION=8.191.12-r0 \
    CA_CERTIFICATES_VERSION=20190108-r0 \
    \
    JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk

ENV PATH=$PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin

RUN apk add --no-cache --upgrade \
        --repository $EDGE_MAIN_REPOSITORY \
        --repository $EDGE_COMMUNITY_REPOSITORY && \
    apk add --no-cache \
        --repository $EDGE_MAIN_REPOSITORY \
        --repository $EDGE_COMMUNITY_REPOSITORY \
        unzip \
        openssl \
		openjdk8="$OPENJDK_VERSION" \
		ca-certificates="$CA_CERTIFICATES_VERSION" && \
    rm  -rf \
        /var/cache/apk/* \
        /var/tmp/* \
        /tmp/*
