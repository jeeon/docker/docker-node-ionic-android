#
# Install OpenJDK and OpenSSL
#

ENV OPENJDK_8_VERSION=8u181-b13-2~deb9u1 \
    CA_CERTIFICATES_JAVA_VERSION=20170929~deb9u1 \
    \
    JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

ENV PATH=$PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        unzip \
        openssl \
        openjdk-8-jdk="$OPENJDK_8_VERSION" \
        ca-certificates-java="$CA_CERTIFICATES_JAVA_VERSION" && \
    \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf \
        /var/lib/apt/lists/* \
        /var/cache/apt/* \
        /var/tmp/* \
        /tmp/*
