#
# Install Gradle
#

ENV GRADLE_VERSION=4.10.3 \
    GRADLE_URL=https://services.gradle.org/distributions/gradle-4.10.3-bin.zip \
    GRADLE_HOME=/opt/gradle \
    GRADLE_OPTS="-Dorg.gradle.daemon=false"

ENV PATH=$PATH:$GRADLE_HOME/gradle-$GRADLE_VERSION/bin

RUN mkdir -p $GRADLE_HOME && \
    wget -q -O gradle.zip $GRADLE_URL && \
    unzip -q -y -d $GRADLE_HOME gradle.zip && \
    rm gradle.zip
