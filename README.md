# docker-node-ionic-android

Ready-made docker image for building and end-to-end testing of
**IonicJS Apps** on **Android**.

Very specific versions (some non-latest) are used.

- node (using base image jeeon/node-ionic)
    - @ 6-default *(for ionic-1)*
    - @ 6-alpine *(for ionic-1)*
    - @ 8-default *(for ionic-3)*
    - @ 8-alpine *(for ionic-3)*
    - @ 10-default *(for ionic-3)* *(latest)*
    - @ 10-alpine *(for ionic-3)*
- openjdk
    - @ 8.191 (for alpine)
    - @ 8.181 (for stretch)
- gradle
    - @ 4.10.3
- android-sdk
    - tools @ 26.1.1
    - build-tools;28.0.3 @ 28.0.3
    - platform-tools @ 28.0.1
    - platforms;android-28 @ 6
    - extras;android;m2repository @ 47 *(prefer latest)*
    - extras;google;m2repository @ 58 *(prefer latest)*
    - extras;google;google_play_services @ 49 *(prefer latest)*

### Pull from Docker Hub
```
docker pull jeeon/node-ionic-android:6-default
docker pull jeeon/node-ionic-android:6-alpine
docker pull jeeon/node-ionic-android:8-default
docker pull jeeon/node-ionic-android:8-alpine
docker pull jeeon/node-ionic-android:10-default
docker pull jeeon/node-ionic-android:10-alpine
```

### Build from GitLab
```
docker build -t jeeon/node-ionic-android:6-default gitlab.com/jeeon/docker/node-ionic-android/6-default
docker build -t jeeon/node-ionic-android:6-alpine gitlab.com/jeeon/docker/node-ionic-android/6-alpine
docker build -t jeeon/node-ionic-android:8-default gitlab.com/jeeon/docker/node-ionic-android/8-default
docker build -t jeeon/node-ionic-android:8-alpine gitlab.com/jeeon/docker/node-ionic-android/8-alpine
docker build -t jeeon/node-ionic-android:10-default gitlab.com/jeeon/docker/node-ionic-android/10-default
docker build -t jeeon/node-ionic-android:10-alpine gitlab.com/jeeon/docker/node-ionic-android/10-alpine
```

### Run image
```
docker run -it jeeon/node-ionic-android:6-default bash
docker run -it jeeon/node-ionic-android:6-alpine sh
docker run -it jeeon/node-ionic-android:8-default bash
docker run -it jeeon/node-ionic-android:8-alpine sh
docker run -it jeeon/node-ionic-android:10-default bash
docker run -it jeeon/node-ionic-android:10-alpine sh
```

### Use as base image
```Dockerfile
FROM jeeon/node-ionic-android:6-default
FROM jeeon/node-ionic-android:6-alpine
FROM jeeon/node-ionic-android:8-default
FROM jeeon/node-ionic-android:8-alpine
FROM jeeon/node-ionic-android:10-default
FROM jeeon/node-ionic-android:10-alpine
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
