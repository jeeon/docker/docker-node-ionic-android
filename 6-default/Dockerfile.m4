#
# Use Jeeon's node-ionic image
#

FROM jeeon/node-ionic:6-default

include(common/maintainer.dockerfile)

include(common/install-openjdk-debian.dockerfile)

include(common/install-gradle.dockerfile)

include(common/install-android.dockerfile)
