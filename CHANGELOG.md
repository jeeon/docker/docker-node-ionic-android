# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 2019-03-01

### New

- Add docker images for Node 10 (based on stretch and alpine).

### Changed

- Make code more DRY by using `m4` to generate `Dockerfile`s from common snippets.
- Upgrade all system packages.
- Upgrade gradle to v4.10.3.
- Update ca-certificates-java to v20170929~deb9u1 (in debian stretch).

## 2019-01-05

### Changed

- Fresh rebuild to get latest system updates and renew latest android sdk licences.
- Update openjdk-8 to v8u181-b13-2~deb9u1 (in debian stretch) and v8.191.12-r0 (in alpine).
- Update ca-certificates to v20190108-r0 (in alpine).

## 2018-10-13

### Changed

- Update openjdk-8 to v8u181-b13-1~deb9u1 (in debian stretch) and v8.171.11-r2 (in alpine).
- Update ca-certificates to v20180924-r0 (in alpine).
- Update android SDK to v28.

## 2018-05-12

### Changed

- Install grade-4.7

## 2018-05-11

### Changed

- Use updated base image.
- Update openjdk-8 to v8u171-b11-1~deb9u1 (in debian stretch).
- Update ca-certificates to v20171114-r3 (in alpine).

## 2018-04-04

### Changed

- Update docs about preferred dependency versions.

## 2018-04-01

### Changed

- Start keeping a ChangeLog.
- Update openjdk-8 to v8u162-b12-1~deb9u1 (in debian stretch) and v8.161.12-r0 (in alpine).
- Update ca-certificates-java to v20170531+nmu1 (in debian stretch) and v20171114-r0 (in alpine).
